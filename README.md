# Selenium webdriver for python

This project is a simple dockerfile to use selenium in a controlled environment.
You can NOT run this dockerfile directly, for more info, look at the example.

To contribute to this project, read the [contributing instructions](CONTRIBUTING.md).
